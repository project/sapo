<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?php print $head_title; ?></title>
		<?php print $head; ?>
		<?php print $styles; ?>
		<?php print $scripts; ?>
	</head>
	<body class="<?php print $body_classes ?>">
		<!--[if IE 6]><div id="ie6"><![endif]-->
		<!--[if IE]><div id="ie"><![endif]-->
		<div id="containerPage" class="clearfix">
			<div id="banner">
				<h1 id="siteTitle">
					<a href="/"><?php print $site_name; ?></a>
				</h1>
				<div id="mainNav"><?php print $mainNav; ?></div>
			</div>
			<div id="containerColumns">
				<div id="columnMain">
					<div id="content">
					<h2 id="pageTitle"><?php print $title; ?></h2>
						<?php print $messages; ?>
					 	<?php print $tabs; ?>
						<?php print $content; ?>
			
					
					</div>
				</div>
				<div id="columnSide">
					<?php print $left; ?>
					
				</div>
			</div>
			<div id="footer">
				
			  <?php print $footer_message; ?>
			</div>
		</div><!--[if IE 6]></div><![endif]-->
		<!--[if IE]></div><![endif]-->
	</body>
</html>
